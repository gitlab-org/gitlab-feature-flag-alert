# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::FeatureFlagCollection do
  it "enumerates its flags" do
    flag_one = double(:flag_one, test: nil)
    flag_two = double(:flag_two, test: nil)
    collection = described_class.new([flag_one, flag_two])

    collection.each(&:test)

    expect(flag_one).to have_received(:test).once
    expect(flag_two).to have_received(:test).once
  end

  describe "#needing_action" do
    it "returns flags needing action" do
      current_milestone = Gitlab::FeatureFlagAlert::Milestone.new("17.6")
      overdue_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new({
        "name" => "overdue",
        "type" => "beta",
        "milestone" => "17.0"
      })
      flag_needing_action = Gitlab::FeatureFlagAlert::FeatureFlag.new({
        "name" => "needs_action",
        "type" => "beta",
        "milestone" => "17.1"
      })
      flag_not_needing_action = Gitlab::FeatureFlagAlert::FeatureFlag.new({
        "name" => "ok",
        "type" => "beta",
        "milestone" => "17.6"
      })
      blank_milestone = Gitlab::FeatureFlagAlert::FeatureFlag.new({ "milestone" => nil })
      flags = [overdue_flag, flag_needing_action, flag_not_needing_action, blank_milestone]
      collection = described_class.new(flags)

      flags = collection.needing_action(current_milestone)

      expect(flags).to contain_exactly(flag_needing_action)
    end
  end

  describe "#overdue" do
    it "returns overdue flags" do
      current_milestone = Gitlab::FeatureFlagAlert::Milestone.new("17.6")
      overdue_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new({
        "name" => "overdue",
        "type" => "beta",
        "milestone" => "17.0"
      })
      not_overdue_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new({
        "name" => "not_overdue",
        "type" => "beta",
        "milestone" => "17.1"
      })
      blank_milestone = Gitlab::FeatureFlagAlert::FeatureFlag.new({ "milestone" => nil })
      flags = [overdue_flag, not_overdue_flag, blank_milestone]
      collection = described_class.new(flags)

      flags = collection.overdue(current_milestone)

      expect(flags).to contain_exactly(overdue_flag)
    end
  end

  describe "#sort_by_milestone" do
    it "returns flags sorted by milestone in ascending order" do
      flag135 = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.5",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      flag140 = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "old_disabled_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "14.0",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => false
      )
      flag1310 = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.10",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      flag131 = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.1",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      flags = [flag135, flag140, flag1310, flag131]
      collection = described_class.new(flags)

      expect(collection.sort_by_milestone.map(&:milestone).map(&:to_s)).to eq(%w[13.1 13.5 13.10 14.0])
    end
  end
end
