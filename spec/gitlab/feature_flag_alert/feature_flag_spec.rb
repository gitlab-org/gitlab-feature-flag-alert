# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::FeatureFlag do
  let(:type) { "beta" }
  let(:current_milestone) { "17.0" }
  let(:params) do
    {
      "name" => "new_pages_ui",
      "feature_issue_url" => "https://gitlab.com/gitlab-org/gitlab/-/issues/454320",
      "introduced_by_url" => "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148534",
      "rollout_issue_url" => "https://gitlab.com/gitlab-org/gitlab/-/issues/468694",
      "milestone" => "17.0",
      "group" => "group::knowledge",
      "type" => type,
      "default_enabled" => true
    }
  end

  let(:flag) do
    described_class.new(params)
  end

  using RSpec::Parameterized::TableSyntax

  describe ".load_all" do
    let(:path_a) { File.join("spec/support/fixtures/configs/config_a/**/*.yml") }
    let(:path_b) { File.join("spec/support/fixtures/configs/config_b/**/*.yml") }
    let(:paths) { [path_a, path_b] }
    let(:flags) { described_class.load_all(*paths) }

    it "supports loading multiple directories" do
      expect(flags.count).to eq(paths.length)
    end

    it "assigns feature_issue_url" do
      expect(flags.count(&:feature_issue_url)).to eq(1)
    end
  end

  it "allows reading all attributes" do
    expect(flag.name).to eq("new_pages_ui")
    expect(flag.feature_issue_url).to eq("https://gitlab.com/gitlab-org/gitlab/-/issues/454320")
    expect(flag.introduced_by_url).to eq("https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148534")
    expect(flag.rollout_issue_url).to eq("https://gitlab.com/gitlab-org/gitlab/-/issues/468694")
    expect(flag.milestone).to eq(Gitlab::FeatureFlagAlert::Milestone.new("17.0"))
    expect(flag.group).to eq("group::knowledge")
    expect(flag.type).to eq("beta")
    expect(flag.default_enabled).to eq(true)
  end

  describe "#milestone_needing_action" do
    subject { flag.milestone_needing_action }

    where(:type, :expected_milestone) do
      "gitlab_com_derisk" | "17.1"
      "wip" | "17.3"
      "beta" | "17.5"
      "ops" | "17.11"
      "experiment" | "17.5"
      "development" | "17.5"
    end

    with_them do
      it { is_expected.to eq(expected_milestone) }
    end
  end

  describe "#end_of_life_milestone" do
    subject { flag.end_of_life_milestone }

    where(:type, :expected_milestone) do
      "gitlab_com_derisk" | "17.2"
      "wip" | "17.4"
      "beta" | "17.6"
      "ops" | "18.0"
      "experiment" | "17.6"
      "development" | "17.6"
    end

    with_them do
      it { is_expected.to eq(expected_milestone) }
    end
  end
end
