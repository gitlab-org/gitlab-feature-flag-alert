# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Milestone do
  describe "#ago" do
    context "when current milestone minor is 0" do
      it "returns the milestone title from N milestones ago" do
        milestone = described_class.new("13.0")

        old_milestone = milestone.ago(4)

        expect(old_milestone).to eq("12.9")
      end
    end

    context "when current milestone minor > 1 but < N" do
      it "returns the correct old milestone title from N milestones ago" do
        milestone = described_class.new("15.5")

        old_milestone = milestone.ago(6)

        expect(old_milestone).to eq("14.12")
      end
    end

    context "when current milestone minor == N" do
      it "returns the correct old milestone title from N milestones ago" do
        milestone = described_class.new("15.6")

        old_milestone = milestone.ago(6)

        expect(old_milestone).to eq("15.0")
      end
    end

    context "when current milestone minor > N" do
      it "returns the correct old milestone title from N milestones ago" do
        milestone = described_class.new("15.7")

        old_milestone = milestone.ago(6)

        expect(old_milestone).to eq("15.1")
      end
    end
  end

  describe "#+" do
    it "returns the milestone title from N milestones in the future when current milestone minor is 0'" do
      milestone = described_class.new("13.0")

      old_milestone = milestone + 4

      expect(old_milestone).to eq("13.4")
    end

    it "returns the next major milestone when milestone rolls over 11" do
      milestone = described_class.new("16.11")

      old_milestone = milestone + 1

      expect(old_milestone).to eq("17.0")
    end

    it "returns the correct milestone when landing on 11" do
      milestone = described_class.new("15.5")

      old_milestone = milestone + 6

      expect(old_milestone).to eq("15.11")
    end

    it "returns the correct milestone when adding 12" do
      milestone = described_class.new("17.0")

      old_milestone = milestone + 13

      expect(old_milestone).to eq("18.1")
    end

    it "works correctly when rolling over the modulo multiple times" do
      milestone = described_class.new("17.0")

      old_milestone = milestone + 24

      expect(old_milestone).to eq("19.0")
    end
  end

  describe "#<=>" do
    it "compares milestones as gitlab versions" do
      milestone = described_class.new("13.11")

      expect(milestone).to be < "14.0"
      expect(milestone).to be > "13.10"
      expect(milestone).to eq "13.11"
    end
  end
end
