# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::GitlabService do
  let(:issue) do
    Gitlab::FeatureFlagAlert::Issue.new(
      "group::code review",
      { needs_action: [], overdue: [] },
      nil
    )
  end

  before do
    allow(Date).to receive(:today).and_return(Date.new(2222, 5, 22))
    stub_const("ENV", ENV.to_hash.merge("TRIAGE_PROJECT_ID" => "123"))
  end

  describe "#current_milestone" do
    context "when one or more active XX.YY milestones exist but non-XX.YY milestone included" do
      before do
        stub_request(:get, "https://gitlab.com/api/v4/groups/9970/milestones?state=active&updated_after=#{(Date.today - 365).iso8601}")
          .with(headers: {
            "Accept": "*/*",
            "Accept-Encoding": "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
            "User-Agent": "Faraday v2.7.11"
          })
          .to_return(
            status: 200,
            headers: {},
            body: <<-JSON
              [
                {
                  "id": 1633524,
                  "iid": 80,
                  "group_id": 9970,
                  "title": "16.0",
                  "description": "",
                  "state": "active",
                  "created_at": "2222-05-30T16:25:25.778Z",
                  "updated_at": "2222-05-30T16:25:25.778Z",
                  "due_date": "2223-05-17",
                  "start_date": "2223-04-18",
                  "expired": false,
                  "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/80"
                },
                {
                  "id": 2552237,
                  "iid": 78,
                  "group_id": 9970,
                  "title": "ClickHouse Acceleration",
                  "description": "",
                  "state": "active",
                  "created_at": "2222-04-06T01:57:34.972Z",
                  "updated_at": "2222-04-06T02:03:45.193Z",
                  "due_date": "2222-06-22",
                  "start_date": "2222-04-06",
                  "expired": false,
                  "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/78"
                },
                {
                  "id": 2453024,
                  "iid": 74,
                  "group_id": 9970,
                  "title": "15.2",
                  "description": "",
                  "state": "active",
                  "created_at": "2222-02-03T22:24:46.818Z",
                  "updated_at": "2222-02-03T22:26:32.010Z",
                  "due_date": "2222-07-17",
                  "start_date": "2222-06-18",
                  "expired": false,
                  "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/74"
                },
                {
                  "id": 2453022,
                  "iid": 73,
                  "group_id": 9970,
                  "title": "15.1",
                  "description": "",
                  "state": "active",
                  "created_at": "2222-02-03T22:23:29.335Z",
                  "updated_at": "2222-02-03T22:23:29.335Z",
                  "due_date": "2222-06-17",
                  "start_date": "2222-05-18",
                  "expired": false,
                  "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/73"
                },
                {
                  "id": 2204142,
                  "iid": 72,
                  "group_id": 9970,
                  "title": "14.10",
                  "description": "",
                  "state": "active",
                  "created_at": "2221-08-26T19:12:33.701Z",
                  "updated_at": "2221-08-26T19:12:33.701Z",
                  "due_date": "2222-04-17",
                  "start_date": "2222-03-18",
                  "expired": true,
                  "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/72"
                },
                {
                  "id": 2204140,
                  "iid": 71,
                  "group_id": 9970,
                  "title": "14.9",
                  "description": "",
                  "state": "active",
                  "created_at": "2221-08-26T19:11:46.841Z",
                  "updated_at": "2222-03-22T13:29:17.028Z",
                  "due_date": "2222-03-17",
                  "start_date": "2222-02-18",
                  "expired": true,
                  "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/71"
                }
              ]
            JSON
          )
      end

      it "returns 15.1" do
        expect(subject.current_milestone).to include("title" => "15.1")
      end
    end

    context "when no XX.YY milestone is included" do
      before do
        stub_request(:get, "https://gitlab.com/api/v4/groups/9970/milestones?state=active&updated_after=#{(Date.today - 365).iso8601}")
          .with(headers: {
            "Accept": "*/*",
            "Accept-Encoding": "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
            "User-Agent": "Faraday v2.7.11"
          })
          .to_return(
            status: 200,
            headers: {},
            body: "[]"
          )
      end

      it "returns nil" do
        expect(subject.current_milestone).to eq(nil)
      end
    end
  end

  describe "#retrieve_groups" do
    context "when error" do
      before do
        stub_request(:get, "https://about.gitlab.com/groups.json")
          .with(query: hash_including("PRIVATE-TOKEN"))
          .to_return(status: [404, "Not Found"])
      end

      it "returns error" do
        expect { subject.retrieve_groups }.to raise_error(Gitlab::FeatureFlagAlert::Error)
      end
    end

    context "when successful with both BE and FE EMs" do
      it "returns YAML" do
        stub_request(:get, "https://about.gitlab.com/groups.json")
          .with(query: hash_including("PRIVATE-TOKEN"))
          .to_return(
            status: [200],
            body: {
              group: {
                backend_engineering_managers: ["@username_be"],
                frontend_engineering_managers: ["@username_fe"],
                engineering_managers: ["@username_manager"]
              }
            }.to_json
          )

        expect(subject.retrieve_groups).to eq({
          "group" => {
            "backend_engineering_managers" => ["@username_be"],
            "frontend_engineering_managers" => ["@username_fe"],
            "engineering_managers" => ["@username_manager"]
          }
        })
      end
    end

    context "when successful with 1 combined EM" do
      it "returns YAML" do
        stub_request(:get, "https://about.gitlab.com/groups.json")
          .with(query: hash_including("PRIVATE-TOKEN"))
          .to_return(status: [200], body: { group: { engineering_manager: ["@username_em"] } }.to_json)

        expect(subject.retrieve_groups).to eq({ "group" => { "engineering_manager" => ["@username_em"] } })
      end
    end
  end

  describe "#create_issue" do
    context "when error" do
      before do
        stub_request(:post, "https://gitlab.com/api/v4/projects/#{ENV['TRIAGE_PROJECT_ID']}/issues")
          .to_return(status: [500, "Internal Server Error"])
      end

      it "returns error" do
        expect { subject.create_issue(issue) }.to raise_error(Gitlab::FeatureFlagAlert::Error)
      end
    end

    context "when successful" do
      it "returns successful" do
        stub_request(:post, "https://gitlab.com/api/v4/projects/#{ENV['TRIAGE_PROJECT_ID']}/issues")
          .to_return(status: 200, body: "{}")

        expect(subject.create_issue(issue)).to eq({})
      end
    end
  end
end
