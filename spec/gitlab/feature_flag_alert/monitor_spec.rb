# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Monitor do
  let(:monitor) { described_class.new }
  let(:gitlab_service) { double(:gitlab_service) }

  before do
    allow(monitor).to receive(:gitlab).and_return(gitlab_service)
  end

  describe "#run" do
    subject { monitor.run }

    let(:feature_flags) do
      Gitlab::FeatureFlagAlert::FeatureFlagCollection.new(
        [
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "needs_action_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.7",
            "type" => "development",
            "group" => "group::code review",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "old_disabled_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.5",
            "type" => "development",
            "group" => "group::code review",
            "default_enabled" => false
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "overdue_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.6",
            "type" => "development",
            "group" => "group::code review",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "needs_action_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.7",
            "type" => "development",
            "group" => "group::pipeline authoring",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "old_disabled_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.5",
            "type" => "development",
            "group" => "group::pipeline authoring",
            "default_enabled" => false
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "overdue_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.6",
            "type" => "development",
            "group" => "group::pipeline authoring",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "new_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "14.0",
            "type" => "development",
            "group" => "group::pipeline execution",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "needs_action_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.7",
            "type" => "development",
            "group" => "group::threat insights",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "old_disabled_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.5",
            "type" => "development",
            "group" => "group::threat insights",
            "default_enabled" => false
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "overdue_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.6",
            "type" => "development",
            "group" => "group::threat insights",
            "default_enabled" => true
          )
        ]
      )
    end

    let(:ce_ff_path) { "#{described_class::WORK_DIR}/config/feature_flags/**/*.yml" }
    let(:ee_ff_path) { "#{described_class::WORK_DIR}/ee/config/feature_flags/**/*.yml" }

    before do
      allow(gitlab_service).to receive(:current_milestone).and_return({ "title" => "14.0" })
      allow(gitlab_service).to receive(:retrieve_groups).and_return(assignees_by_group)
      expect(Gitlab::FeatureFlagAlert::FeatureFlag).to receive(:load_all).with(ce_ff_path, ee_ff_path)
                                                                         .and_return(feature_flags)
    end

    shared_examples "creates one issue for group with overdue flag" do |label, assignees|
      it "creates issue for #{label} with correct assignees and grouped flags" do
        issues = subject

        issues_for_group = issues.select { |i| i.group == label }

        expect(issues_for_group.size).to eq(1)

        issue = issues_for_group.first

        expect(issue.flags[:overdue].map(&:name)).to eq(%w[old_disabled_feature_flag overdue_feature_flag])
        expect(issue.flags[:needs_action].map(&:name)).to eq(%w[needs_action_feature_flag])
        expect(issue.assignees).to eq(assignees)
      end
    end

    context "when group has backend_engineering_manager" do
      let(:assignees_by_group) do
        {
          "code_review" => {
            "label" => "group::code review",
            "backend_engineering_managers" => ["alice"],
            "frontend_engineering_managers" => ["bob"]
          },
          "pipeline_authoring" => {
            "label" => "group::pipeline authoring foo",
            "extra_labels" => ["group::pipeline authoring"],
            "backend_engineering_managers" => ["john"],
            "engineering_managers" => ["jane"]
          },
          "threat_insights" => {
            "label" => "group::threat insights",
            "fullstack_managers" => ["charles"]
          }
        }
      end

      it_behaves_like "creates one issue for group with overdue flag", "group::code review", "@alice @bob"
      it_behaves_like "creates one issue for group with overdue flag", "group::pipeline authoring", "@john @jane"
      it_behaves_like "creates one issue for group with overdue flag", "group::threat insights", "@charles"
    end
  end
end
