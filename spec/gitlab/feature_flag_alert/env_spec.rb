# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Env do
  before do
    stub_const("ENV", ENV.to_hash.merge(env))
  end

  describe "#create_issue?" do
    let(:env) { { "CREATE_ISSUE" => value } }

    subject { described_class.create_issue? }

    context "when set to true" do
      let(:value) { "true" }

      it { is_expected.to eq(true) }
    end

    context "when in test mode" do
      let(:value) { nil }

      before do
        allow(described_class).to receive(:create_issue_test_mode?).and_return(true)
      end

      it { is_expected.to eq(true) }
    end

    context "when empty" do
      let(:value) { nil }

      it { is_expected.to eq(false) }
    end

    context "when unknown" do
      let(:value) { "1" }

      it { is_expected.to eq(false) }
    end
  end

  describe "#create_issue_test_mode?" do
    let(:env) { { "CREATE_ISSUE" => value } }

    subject { described_class.create_issue_test_mode? }

    context "when set to test" do
      let(:value) { "test" }

      it { is_expected.to eq(true) }
    end

    context "when empty" do
      let(:value) { nil }

      it { is_expected.to eq(false) }
    end

    context "when unknown" do
      let(:value) { "1" }

      it { is_expected.to eq(false) }
    end
  end

  describe "#triage_project_id" do
    let(:env_project_id) { "123" }
    let(:env) { { "TRIAGE_PROJECT_ID" => env_project_id } }

    subject { described_class.triage_project_id }

    before do
      allow(described_class).to receive(:create_issue_test_mode?).and_return(test_mode)
    end

    context "when in test mode" do
      let(:test_mode) { true }

      it { is_expected.to eq(described_class::ENGINEERING_PRODUCTIVITY_TRIAGE_OPS_PLAYGROUND_PROJECT_ID) }
    end

    context "when not in test mode" do
      let(:test_mode) { false }

      it { is_expected.to eq(env_project_id) }
    end
  end
end
