# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Issue do
  let(:current_milestone) { Gitlab::FeatureFlagAlert::Milestone.new }
  let(:group) { "group::code review" }
  let(:flags) do
    {
      needs_action: [
        Gitlab::FeatureFlagAlert::FeatureFlag.new({
          "name" => "flag_one",
          "introduced_by_url" => "http://merge_request.com",
          "rollout_issue_url" => "http://issue.com",
          "feature_issue_url" => "http://feature-issue.com",
          "milestone" => "11.7",
          "type" => "development",
          "group" => "group::code review",
          "default_enabled" => true
        })
      ],
      overdue: [
        Gitlab::FeatureFlagAlert::FeatureFlag.new({
          "name" => "flag_two",
          "introduced_by_url" => nil,
          "rollout_issue_url" => nil,
          "milestone" => "12.0",
          "type" => "development",
          "group" => "group::code review",
          "default_enabled" => false
        }),
        Gitlab::FeatureFlagAlert::FeatureFlag.new({
          "name" => "flag_three",
          "introduced_by_url" => nil,
          "rollout_issue_url" => "http://nope.com",
          "milestone" => "12.0",
          "type" => "development",
          "group" => "group::code review",
          "default_enabled" => true
        })
      ]
    }
  end

  subject { Gitlab::FeatureFlagAlert::Issue.new(group, flags, nil) }

  describe "#body" do
    let(:ci_job_url) { "https://gitlab.example.com/project/namespace/-/jobs/1234567890" }

    before do
      allow(Gitlab::FeatureFlagAlert::Env).to receive(:ci_job_url).and_return(ci_job_url)
    end

    it "generates the issue description in markdown" do
      expectation = <<~STRING
        This is a group level feature flag report containing feature flags that should be evaluated or need action.

        Feature flag trends can be found in [this dashboard](#{described_class::DASHBOARD_URL}).

        A feature flag is considered end-of-life when it has existed in the codebase for longer than
        the prescribed lifespan based on [feature flag type](https://docs.gitlab.com/ee/development/feature_flags/#types-of-feature-flags).

        # Feature flags needing action

        These flags are approaching end-of-life in the next milestone.

        | Feature flag | Milestone added | End of life milestone | Enabled by default? | Rollout issue |
        | ------------ | --------------- | --------------------- | ------------------- | ------------- |
        | `flag_one` <br> {+development+} \\| [Introduced by](http://merge_request.com) \\| [Feature issue](http://feature-issue.com) \\| [GitLab.com state changes](https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues?search=flag_one&sort=created_date&state=all&label_name[]=host::gitlab.com) | %11.7 | %12.1 | :white_check_mark: | http://issue.com+s |

        Please take action on these feature flags by performing one of the following options:

        1. Enable the feature flag by default and remove it.
        1. Convert it to an instance, group, or project setting.
        1. Revert the changes if it's still disabled and not needed anymore.

        # Feature flags overdue

        These flags have reached their maximum lifespan.

        | Feature flag | Milestone added | End of life milestone | Enabled by default? | Rollout issue |
        | ------------ | --------------- | --------------------- | ------------------- | ------------- |
        | `flag_two` <br> {+development+} \\| Introduced by ? \\| No feature issue \\| [GitLab.com state changes](https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues?search=flag_two&sort=created_date&state=all&label_name[]=host::gitlab.com) | %12.0 | %12.6 | :x: |  |
        | `flag_three` <br> {+development+} \\| Introduced by ? \\| No feature issue \\| [GitLab.com state changes](https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues?search=flag_three&sort=created_date&state=all&label_name[]=host::gitlab.com) | %12.0 | %12.6 | :white_check_mark: | http://nope.com+s |

        Please review these feature flags to determine if they are able to be removed entirely.

        ----

        _This report is generated from [feature-flag-alert project](https://gitlab.com/gitlab-org/gitlab-feature-flag-alert/) by #{ci_job_url}._

        /label ~"group::code review" ~"feature flag" ~"triage report"
        /assign @gl-dx/eng-prod
        /due in 1 month
        /relate http://issue.com
        /relate http://nope.com
      STRING

      expect(subject.body).to eq(expectation)
    end

    context "with ci job URL set" do
      let(:ci_job_url) { nil }

      it "shows `manually`" do
        expect(subject.body).to include(" manually._")
      end
    end

    context "without related rollout issues" do
      before do
        flags.each_value do |flag_list|
          flag_list.each { |flag| flag.rollout_issue_url = nil }
        end
      end

      it "does not relate issues" do
        expect(subject.body).not_to include("/relate")
      end
    end
  end

  describe "#title" do
    it "includes group and date" do
      expect(subject.title).to include(Date.today.iso8601)
      expect(subject.title).to include("group::code review")
    end
  end
end
