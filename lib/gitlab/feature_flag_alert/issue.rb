# frozen_string_literal: true

require "date"

require "gitlab/feature_flag_alert/env"

module Gitlab
  module FeatureFlagAlert
    class Issue
      DASHBOARD_URL = "https://10az.online.tableau.com/#/site/gitlab/views/Engineering-Featureflags_17297013435940/Engineering-FeatureFlags"
      DEFAULT_ASSIGNEE = "@gl-dx/eng-prod"

      attr_reader :group, :flags, :assignees

      def initialize(group, flags, assignees)
        @group = group
        @flags = flags
        @assignees = assignees || DEFAULT_ASSIGNEE
      end

      def title
        "Feature flags requiring attention for #{group} - #{Date.today.iso8601}"
      end

      def body
        <<~SUMMARY
          This is a group level feature flag report containing feature flags that should be evaluated or need action.

          Feature flag trends can be found in [this dashboard](#{DASHBOARD_URL}).

          A feature flag is considered end-of-life when it has existed in the codebase for longer than
          the prescribed lifespan based on [feature flag type](https://docs.gitlab.com/ee/development/feature_flags/#types-of-feature-flags).

          # Feature flags needing action

          These flags are approaching end-of-life in the next milestone.

          #{table(flags[:needs_action])}

          Please take action on these feature flags by performing one of the following options:

          1. Enable the feature flag by default and remove it.
          1. Convert it to an instance, group, or project setting.
          1. Revert the changes if it's still disabled and not needed anymore.

          # Feature flags overdue

          These flags have reached their maximum lifespan.

          #{table(flags[:overdue])}

          Please review these feature flags to determine if they are able to be removed entirely.

          ----

          _This report is generated from [feature-flag-alert project](https://gitlab.com/gitlab-org/gitlab-feature-flag-alert/) #{generation_source}._

          /label ~"#{group}" ~"feature flag" ~"triage report"
          /assign #{assignees}
          /due in 1 month
          #{relate_rollout_issues}
        SUMMARY
      end

      private

      def table(flags)
        content = []
        content << "| Feature flag | Milestone added | End of life milestone | Enabled by default? | Rollout issue |"
        content << "| ------------ | --------------- | --------------------- | ------------------- | ------------- |"

        flags.each do |flag|
          content << "| #{feature_flag_info(flag)} | %#{flag.milestone} | %#{flag.end_of_life_milestone} | #{default_status(flag)} | #{rollout_issue_url(flag)} |"
        end

        content.join("\n")
      end

      def feature_flag_info(flag)
        info = []
        info << "`#{flag.name}`"
        info << [
          "{+#{flag.type}+}",
          introduced_by_url(flag),
          feature_issue_url(flag),
          log_issues(flag)
        ].join(" \\| ")

        info.join(" <br> ")
      end

      def default_status(flag)
        if flag.default_enabled
          ":white_check_mark:"
        else
          ":x:"
        end
      end

      def introduced_by_url(flag)
        flag.introduced_by_url ? "[Introduced by](#{flag.introduced_by_url})" : "Introduced by ?"
      end

      def rollout_issue_url(flag)
        return unless flag.rollout_issue_url

        "#{flag.rollout_issue_url}+s"
      end

      def feature_issue_url(flag)
        flag.feature_issue_url ? "[Feature issue](#{flag.feature_issue_url})" : "No feature issue"
      end

      def generation_source
        url = Env.ci_job_url

        url ? "by #{url}" : "manually"
      end

      def relate_rollout_issues
        flags
          .flat_map { |_type, flag| flag }
          .filter_map { |flag| "/relate #{flag.rollout_issue_url}" if flag.rollout_issue_url }
          .join("\n")
      end

      def log_issues(flag)
        url = "https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues?search=#{flag.name}&sort=created_date&state=all&label_name[]=host::gitlab.com"

        "[GitLab.com state changes](#{url})"
      end
    end
  end
end
