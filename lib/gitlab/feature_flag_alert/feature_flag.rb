# frozen_string_literal: true

require "yaml"
require "gitlab/feature_flag_alert/feature_flag_collection"
require "gitlab/feature_flag_alert/milestone"

module Gitlab
  module FeatureFlagAlert
    class FeatureFlag
      ATTRIBUTES = %i[name introduced_by_url feature_issue_url rollout_issue_url type group default_enabled].freeze

      LIFESPANS_BY_TYPE = {
        # https://docs.gitlab.com/ee/development/feature_flags/#types-of-feature-flags
        "gitlab_com_derisk" => 2,
        "wip" => 4,
        "beta" => 6,
        "ops" => 12,
        "experiment" => 6,
        "development" => 6
      }.freeze

      attr_accessor(*ATTRIBUTES)

      def self.load_all(*dir_paths)
        paths = dir_paths.flat_map { |p| Dir.glob(p) }

        flags = paths.map do |path|
          params = YAML.load_file(path)

          FeatureFlag.new(params)
        end

        FeatureFlagCollection.new(flags)
      end

      def initialize(params)
        ATTRIBUTES.each do |attribute|
          public_send("#{attribute}=", params[attribute.to_s])
        end

        @params = params
      end

      def milestone
        @milestone ||= Milestone.new(params["milestone"])
      end

      def milestone_needing_action
        milestone + (lifespan_in_milestones - 1)
      end

      def end_of_life_milestone
        milestone + lifespan_in_milestones
      end

      private

      attr_reader :params

      def lifespan_in_milestones
        # If we don't know the type then we'll default to 2 months
        # since issues would be better addressed sooner rather than later.
        LIFESPANS_BY_TYPE.fetch(type, 2)
      end
    end
  end
end
