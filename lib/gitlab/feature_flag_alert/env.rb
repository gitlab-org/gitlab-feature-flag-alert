# frozen_string_literal: true

module Gitlab
  module FeatureFlagAlert
    module Env
      module_function

      # https://gitlab.com/gitlab-org/quality/engineering-productivity/triage-ops-playground
      ENGINEERING_PRODUCTIVITY_TRIAGE_OPS_PLAYGROUND_PROJECT_ID = 36_072_369

      def create_issue?
        ENV["CREATE_ISSUE"] == "true" || create_issue_test_mode?
      end

      def create_issue_test_mode?
        ENV["CREATE_ISSUE"] == "test"
      end

      def create_issue_dry_run?
        ENV["CREATE_ISSUE"] == "dry-run"
      end

      def triage_project_id
        create_issue_test_mode? ? ENGINEERING_PRODUCTIVITY_TRIAGE_OPS_PLAYGROUND_PROJECT_ID : ENV["TRIAGE_PROJECT_ID"]
      end

      def ci_job_url
        ENV["CI_JOB_URL"]
      end

      def gitlab_token
        ENV.fetch("GITLAB_TOKEN")
      end
    end
  end
end
