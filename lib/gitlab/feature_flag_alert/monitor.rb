# frozen_string_literal: true

require "gitlab/feature_flag_alert/env"
require "gitlab/feature_flag_alert/feature_flag"
require "gitlab/feature_flag_alert/gitlab_service"
require "gitlab/feature_flag_alert/issue"
require "gitlab/feature_flag_alert/milestone"

module Gitlab
  module FeatureFlagAlert
    class Monitor
      GITLAB_GIT_REPO = "https://gitlab.com/gitlab-org/gitlab.git"
      WORK_DIR = "gitlab-repo"

      def run
        update_repository

        current_milestone = Milestone.new(gitlab.current_milestone["title"])
        puts "Current milestone: #{current_milestone}"

        flags_needing_action_by_group = feature_flags
                                        .needing_action(current_milestone)
                                        .group_by(&:group)

        flags_overdue_by_group = feature_flags
                                 .overdue(current_milestone)
                                 .group_by(&:group)

        groups = (flags_needing_action_by_group.keys + flags_overdue_by_group.keys).uniq

        flags_by_group = groups.reduce({}) do |flags, group|
          flags.update(
            group => {
              needs_action: flags_needing_action_by_group.fetch(group, []),
              overdue: flags_overdue_by_group.fetch(group, [])
            }
          )
        end

        flags_by_group.map do |group, flags|
          next if group.nil?

          flags_needing_action = flags[:needs_action]
          flags_overdue = flags[:overdue]

          puts "Feature flags needing action for #{group}:"
          flags_needing_action.each do |flag|
            puts " * #{flag.name} | #{flag.type} | #{flag.milestone} | #{flag.default_enabled} | #{flag.introduced_by_url} | #{flag.rollout_issue_url}"
          end

          puts ""

          puts "Feature flags overdue for #{group}:"
          flags_overdue.each do |flag|
            puts " * #{flag.name} | #{flag.milestone} | #{flag.default_enabled} | #{flag.rollout_issue_url}"
          end

          puts ""

          issue = build_issue(group, flags)
          handle_issue(issue)

          # In test mode only create a single issue
          break issue if Env.create_issue_test_mode?

          issue
        end
      end

      private

      def feature_flags
        @feature_flags ||= FeatureFlag.load_all(*feature_flag_dirs).sort_by_milestone
      end

      def feature_flag_dirs
        [File.join(WORK_DIR, "config/feature_flags/**/*.yml")].tap do |dirs|
          ee_config_dir = File.join(WORK_DIR, "ee/config/feature_flags")

          dirs << File.join(ee_config_dir, "**/*.yml") if Dir.exist?(ee_config_dir)
        end
      end

      def gitlab
        @gitlab ||= GitlabService.new
      end

      def groups
        @groups ||= gitlab.retrieve_groups
      end

      def update_repository
        if File.exist?(WORK_DIR)
          `cd #{WORK_DIR} && git fetch --depth 1 && git reset --hard origin/master && git clean -dfx`
        else
          `git clone --depth=1 #{GITLAB_GIT_REPO} #{WORK_DIR}`
        end
      end

      def group_definition(group_label)
        groups.find do |_, group_data|
          group_data["label"] == group_label || group_data["extra_labels"]&.include?(group_label)
        end
      end

      def prepared_assignees(group_label)
        # Unknown group
        return if group_label.nil?

        _, group = group_definition(group_label)

        # No group defined based on the group:: label
        return if group.nil?

        prepared = group.values_at(
          "backend_engineering_managers",
          "frontend_engineering_managers",
          "fullstack_managers",
          "engineering_managers"
        ).flatten.compact.uniq

        # No DRI
        return if prepared.empty?

        prepared.map { |username| "@#{username}" }.join(" ")
      end

      def build_issue(group, flags)
        assignees = Env.create_issue_test_mode? ? "" : prepared_assignees(group)

        Issue.new(group, flags, assignees)
      end

      def handle_issue(issue)
        if Env.create_issue?
          gitlab.create_issue(issue)
        elsif Env.create_issue_dry_run?
          warn "Title: #{issue.title}"
          warn "Body:"
          warn issue.body
          warn "=" * 80
          warn
        end
      end
    end
  end
end
