# frozen_string_literal: true

require "faraday"
require "json"
require "yaml"
require "date"
require "gitlab/feature_flag_alert/env"

module Gitlab
  module FeatureFlagAlert
    class GitlabService
      BASE_URL = "https://gitlab.com/api/v4"
      GITLAB_ORG_GROUP_ID = 9970
      GITLAB_GROUP_DEFINITIONS = "https://about.gitlab.com/groups.json"

      def current_milestone
        response = Faraday.get(BASE_URL + "/groups/#{GITLAB_ORG_GROUP_ID}/milestones", { state: :active, updated_after: (Date.today - 365).iso8601 }, http_headers)

        raise Error, response.body unless response.success?

        milestones = JSON.parse(response.body)

        milestones.find do |milestone|
          !milestone["expired"] &&
          milestone["start_date"] && milestone["due_date"] &&
          Date.parse(milestone["start_date"]) <= today &&
          Date.parse(milestone["due_date"]) >= today &&
          milestone["title"].match?(/\A\d+\.\d+\z/)
        end
      end

      def retrieve_groups
        response = Faraday.get(GITLAB_GROUP_DEFINITIONS, http_headers)

        raise Error, response.body unless response.success?

        JSON.parse(response.body)
      end

      def create_issue(issue)
        response = Faraday.post(BASE_URL + "/projects/#{Env.triage_project_id}/issues",
          {
            description: issue.body,
            title: issue.title
          }, http_headers)

        raise Error, response.body unless response.success?

        JSON.parse(response.body)
      end

      private

      def http_headers
        { "PRIVATE-TOKEN" => Env.gitlab_token }
      end

      def today
        @today ||= Date.today
      end
    end
  end
end
