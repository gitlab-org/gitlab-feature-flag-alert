# frozen_string_literal: true

module Gitlab
  module FeatureFlagAlert
    class FeatureFlagCollection
      include Enumerable

      def initialize(flags)
        @flags = flags
      end

      def needing_action(current_milestone)
        self.class.new(flags_needing_action(current_milestone))
      end

      def overdue(current_milestone)
        self.class.new(flags_overdue(current_milestone))
      end

      def sort_by_milestone
        self.class.new(flags.sort_by(&:milestone))
      end

      def each(&block)
        flags.each do |flag|
          block.call(flag)
        end
      end

      private

      attr_reader :flags

      def flags_needing_action(current_milestone)
        flags.select do |flag|
          flag.milestone.present? &&
            current_milestone >= flag.milestone_needing_action &&
            current_milestone < flag.end_of_life_milestone
        end
      end

      def flags_overdue(current_milestone)
        flags.select do |flag|
          flag.milestone.present? &&
            current_milestone >= flag.end_of_life_milestone
        end
      end
    end
  end
end
