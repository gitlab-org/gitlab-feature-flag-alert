# frozen_string_literal: true

require "gitlab/feature_flag_alert/version"
require "gitlab/feature_flag_alert/monitor"
require "gitlab/feature_flag_alert/issue"

module Gitlab
  module FeatureFlagAlert
    class Error < StandardError; end
  end
end
