# Gitlab::FeatureFlagAlert

This tool looks for outdated feature flags in the `gitlab-org/gitlab` project and creates an issue for each GitLab group with outdated feature flags.

Issues are then created for each GitLab group (team) that lists all their outdated feature flags.
The issues are assigned to each group's Engineering Manager.

The issues do not include:

- Feature flags created in the last 2 milestones.
- Feature flags of types other than `development`. For example, `ops` feature flags are not included.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab-feature_flag_alert', git: 'https://gitlab.com/gitlab-org/gitlab-feature-flag-alert'
```

And then execute:

```
bundle install
```


## Usage

Set ENV variables:

- (required) `ENV['GITLAB_TOKEN']`: Your access token for access to the API.
- (optional) `ENV['CREATE_ISSUE']`:
    - `true` to create 1 issue per group.
    - `test` to create a single test issue in https://gitlab.com/gitlab-org/quality/engineering-productivity/triage-ops-playground without any assignments
    - `dry-run` to print issue title and description to STDERR
- (optional) `ENV['TRIAGE_PROJECT_ID']`: Created the issues in this project.

To run the tool on the CLI:

```
rm -rf gitlab-repo/ # make sure the directory is empty
GITLAB_TOKEN=**** bundle exec bin/feature-flag-alert
```

Test run to create a single report in https://gitlab.com/gitlab-org/quality/engineering-productivity/triage-ops-playground without any assignees.

```
rm -rf gitlab-repo/ # make sure the directory is empty
GITLAB_TOKEN=**** CREATE_ISSUE=test bundle exec bin/feature-flag-alert
```

To dry-run all reports and print issue titles and descriptions run:

```
rm -rf gitlab-repo/ # make sure the directory is empty
GITLAB_TOKEN=**** CREATE_ISSUE=dry-run bundle exec bin/feature-flag-alert 2> dry-run.txt
```

To run the tool as a Gem:

```ruby
Gitlab::FeatureFlagAlert::Monitor.new.run
```

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/gitlab-org/gitlab-feature-flag-alert

The tool was originally implemented by [@fabiopitino](https://gitlab.com/fabiopitino) and is now
maintained by the [Engineering Productivity team](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/).

### GITLAB_TOKEN CI/CD variable

Merge request pipelines for this project requires a valid `GITLAB_TOKEN` variable. This is a gitlab-bot personal access token. Please see [internal runbook](https://internal.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/rotating-credentials/#rotate-a-gitlab-bot-personal-access-token) for token rotation instructions.
